/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const { resolve } = require( "path" );
const { readFile } = require( "fs" ).promises;

const { error } = require( "../lib/logger" );
const { isDevelopment } = require( "../lib/env" );

const { Backends } = require( "../backends" );
const { LDAPServer } = require( "./ldap" );

const configFile = process.env.CONFIG_FILE || resolve( process.cwd(), "config" );
const config = require( configFile );


let Server = LDAPServer, options = {};

if ( config.server ) {
	Server = LDAPServer;
	options = config.server;
}

( options.certFile && options.keyFile ? Promise.all( [
	readFile( resolve( configFile, "..", options.certFile ) ),
	readFile( resolve( configFile, "..", options.keyFile ) ),
] ) : Promise.resolve( [] ) )
	.then( ( [ cert, key ] ) => {
		options.cert = cert;
		options.key = key;

		const backends = new Backends( config );

		return new Server( config, backends ).listening;
	} )
	.catch( exception => {
		error( `service failed on exception: ${isDevelopment ? exception.stack : exception.message}` );
	} );
