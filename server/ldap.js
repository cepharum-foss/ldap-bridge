/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const DNS = require( "dns" ).promises;

const LDAP = require( "ldapjs" );
const { debug, info, error } = require( "../lib/logger" );

const { isDevelopment } = require( "../lib/env" );

const DefaultUsernameAttributes = [ "mail", "mailpublic", "uid", "user", "username", "login", "loginname" ];


/**
 * @typedef {EqualityFilter|PresenceFilter|SubstringFilter|GreaterThanEqualsFilter|LessThanEqualsFilter|ApproximateFilter} LDAPBasicFilter
 * @typedef {AndFilter|OrFilter|NotFilter} LDAPLogicFilter
 * @typedef {LDAPBasicFilter|LDAPLogicFilter} LDAPFilter
 */

/**
 * Implements LDAP server.
 *
 * @property {string} ip IP address service is bound to for listening
 * @property {number} port port service is listening on
 * @property {LDAP.Server} server running server instance
 * @property {Promise<LDAPServer>} listening promises server listening for incoming requests
 * @property {string[]} acceptedPeers lists domain names of clients permitted to request, if empty all clients may request
 * @property {Backends} backends aggregation of available backends
 */
class LDAPServer {
	/**
	 * @param {object} config service configuration
	 * @param {Backends} backends aggregation of available backends
	 */
	constructor( config, backends ) {
		const { server: options, peers = [] } = config || {};

		if ( !isDevelopment && ( !options.cert || !options.key ) ) {
			throw new TypeError( "missing cert/key for serving LDAP in TLS mode" );
		}


		Object.defineProperties( this, {
			ip: { value: options.ip || "0.0.0.0", enumerable: true },
			port: { value: options.port || ( options.cert ? 636 : 389 ), enumerable: true },
			backends: { value: backends, enumerable: true },
			acceptedPeers: { value: Object.freeze( Array.isArray( peers ) ? peers.slice() : peers ? [peers] : [] ), enumerable: true },
		} );


		let listenResolve, listenReject;
		const listening = new Promise( ( resolve, reject ) => {
			listenResolve = resolve;
			listenReject = reject;
		} );

		info( `creating LDAP server` );

		const server = LDAP.createServer( {
			key: options.key,
			certificate: options.cert,
		} );

		info( `binding to ${this.ip}:${this.port}` );

		server.listen( this.port, this.ip, () => {
			info( `now listening on ${this.ip}:${this.port}` );

			listenResolve( this );
		} );

		server.once( "error", cause => listenReject( cause ) );
		server.once( "end", () => listenReject( new Error( "LDAP server shut down prematurely" ) ) );

		Object.defineProperties( this, {
			server: { value: server, enumerable: true },
			listening: { value: listening },
		} );

		this.defineRoutes();
	}

	/**
	 * Parses mailbox address extracting its components.
	 *
	 * @param {string} mailbox address of mailbox to parse
	 * @param {?string} domain default domain to use in case `mailbox` doesn't name one
	 * @returns {{mailbox: string, sub: string, domain: string}} extracted components of mailbox address
	 * @throws TypeError on providing invalid mailbox address
	 */
	static parseMail( mailbox, domain ) {
		const normalized = String( mailbox ).trim().toLowerCase();

		const parsed = /^([^\s,@+*?]+)(?:\+([^\s,@*?]+))?(?:@((?:[a-z0-9_-]+\.)+(?:[a-z]{2,})))?$/i.exec( normalized );
		if ( !parsed || ( !parsed[3] && !domain ) ) {
			throw new TypeError( `invalid mailbox address: ${mailbox}` );
		}

		return {
			mailbox: parsed[1],
			sub: parsed[2],
			domain: parsed[3] || domain,
		};
	}

	/**
	 * Converts a domain name into related suffix DN.
	 *
	 * @param {string} domain domain name to convert
	 * @returns {string} DN suffix related to provided domain
	 * @throws TypeError on providing invalid domain name
	 */
	static domainToDN( domain ) {
		const normalized = String( domain ).trim().toLowerCase();

		if ( !/^(?:[a-z0-9_-]+\.)+(?:[a-z]{2,})$/i.test( normalized ) ) {
			throw new TypeError( `invalid domain name: ${domain}` );
		}

		return normalized.split( "." ).map( segment => `dc=${segment}` ).join( "," );
	}

	/**
	 * Searches provided filter for some username being searched for.
	 *
	 * @param {LDAPFilter} filter filter to process
	 * @param {?Backend} backend particular backend search will be limited to
	 * @returns {string[]} extracted usernames, might be empty
	 */
	static extractUsernames( filter, backend ) {
		const names = [];
		const extract = f => {
			if ( f instanceof LDAP.EqualityFilter ) {
				if ( DefaultUsernameAttributes.indexOf( f.attribute.trim().toLowerCase() ) > -1 ) {
					const parsed = this.parseMail( f.value, backend ? backend.domain : undefined );

					names.push( {
						attribute: f.attribute,
						mailbox: parsed.mailbox,
						domain: parsed.domain,
						address: `${parsed.mailbox}@${parsed.domain}`,
					} );
				}
			} else if ( f instanceof LDAP.AndFilter || f instanceof LDAP.OrFilter ) {
				f.filters.forEach( extract );
			}
		};

		extract( filter );

		return names;
	}

	/**
	 * Extracts user information from provided DN.
	 *
	 * @param {DN|string} dn DN of user to process
	 * @returns {{mailbox: *, address: string, domain: string, attribute: string}} extracted user information
	 * @throws TypeError on invalid DN
	 */
	static dnToUsername( dn ) {
		const _dn = typeof dn === "string" ? LDAP.parseDN( dn ) : dn;

		_dn.setFormat( { skipSpace: true } );

		const first = _dn.rdns[0];
		const segments = [];

		for ( let i = 1, num = _dn.rdns.length; i < num; i++ ) {
			const rdn = _dn.rdns[i];

			if ( rdn.attrs.dc ) {
				segments.push( rdn.attrs.dc.value );
			} else if ( segments.length > 0 ) {
				if ( i < num - 1 ) {
					throw new TypeError( `invalid DN ${_dn} for extracting username` );
				}
			}
		}

		if ( first && segments.length > 1 ) {
			const attribute = Object.keys( first.attrs )[0];

			if ( attribute && DefaultUsernameAttributes.indexOf( attribute.trim().toLowerCase() ) > -1 ) {
				const mailbox = first.attrs[attribute].value;
				const domain = segments.join( "." );

				return {
					attribute, mailbox, domain,
					address: `${mailbox}@${domain}`,
				};
			}
		}

		throw new TypeError( `invalid DN ${_dn} for extracting username` );
	}

	/**
	 * Qualifies attributes of entry to be returned to a searching LDAP client.
	 *
	 * @param {object} user information on user described by returned entry
	 * @param {object} boundUser information on currently bound LDAP user
	 * @param {object} attributes LDAP attributes to be qualified
	 * @returns {object} qualified LDAP attributes
	 */
	static qualifyAttributes( user, boundUser, attributes ) {
		const match = /^([^@_+]+)\.([^@_+.]+)(?:[_@]|$)/.exec( user.mailbox.trim().toLowerCase() );

		if ( match ) {
			const converter = ( _, lead, letter ) => lead + letter.toUpperCase();
			const overlay = {
				givenName: match[1].trim().replace( /(^|-|\.)([a-z])/g, converter ).replace( /\./g, " " ),
				sn: match[2].trim().replace( /(^|-)([a-z])/g, converter ),
			};

			const flag = user.mailbox.replace( /^.+_/, "" ).trim();
			if ( flag ) {
				overlay.objectclass = ( attributes.objectclass || [] ).concat( flag );
			}

			return Object.assign( {}, attributes, overlay );
		}

		return attributes;
	}

	/**
	 * Registers handlers with server instance.
	 *
	 * @returns {void}
	 * @protected
	 */
	defineRoutes() {
		const { server, backends } = this;

		backends.available.forEach( ( { domain: backendDomain, backend } ) => {
			const backendDN = this.constructor.domainToDN( backendDomain );
			const parsedBackendDN = LDAP.parseDN( backendDN );

			parsedBackendDN.setFormat( { skipSpace: true } );

			server.search( backendDN, ( req, res, next ) => {
				search( req, res, next, backend, `scope of ${backendDN}`, this );
			} );

			server.bind( backendDN, ( req, res, next ) => {
				this.checkPeer( req, backend )
					.then( permitted => {
						if ( !permitted ) {
							next( new LDAP.InsufficientAccessRightsError( "client rejected" ) );
							return undefined;
						}

						req.name.setFormat( { skipSpace: true } );

						debug( `${req.logId}: binding ${req.authentication} as ${req.name} in scope of ${backendDN}` );

						if ( req.authentication !== "simple" ) {
							const msg = "must be simple bind";

							error( `${req.logId}: ${msg}` );
							next( new LDAP.AuthMethodNotSupportedError( msg ) );
							return undefined;
						}

						if ( !req.dn.childOf( parsedBackendDN ) ) {
							error( `${req.logId}: rejecting foreign bind DN ${req.name} in scope of ${backendDN}` );
							next( new LDAP.InvalidDnSyntaxError( "DN must address child node of domain" ) );
							return undefined;
						}

						const rdn = req.dn.rdns[0];
						const attribute = Object.keys( rdn.attrs )[0];
						const value = rdn.attrs[attribute].value;

						if ( DefaultUsernameAttributes.indexOf( attribute.toLowerCase() ) < 0 ) {
							error( `${req.logId}: rejecting invalid attribute ${attribute} on binding in scope of ${backendDN}` );
							next( new LDAP.InvalidDnSyntaxError( "invalid DN attribute" ) );
							return undefined;
						}

						const address = `${value}@${backendDomain}`;

						return backend.exists( address )
							.then( exists => {
								if ( exists ) {
									return backend.authenticate( address, req.credentials )
										.then( () => {
											res.end();
											next();
										} );
								}

								error( `${req.logId}: can't bind as unknown user ${address}` );
								next( new LDAP.NoSuchObjectError( "user not found" ) );

								return undefined;
							} )
							.catch( cause => {
								error( `${req.logId}: invalid credentials rejected on binding as user ${address} in scope of ${backendDN}: ${cause.stack}` );
								next( cause );
							} );
					} )
					.catch( cause => {
						error( `${req.logId}: handling bind request failed: ${cause.stack}` );
						next( new LDAP.OperationsError( cause.message ) );
					} );
			} );
		} );

		server.search( "cn=search", ( req, res, next ) => {
			search( req, res, next, undefined, "all available backends", this );
		} );

		/**
		 * Commonly handles LDAP search requests.
		 *
		 * @param {LDAP.SearchRequest} req LDAP request descriptor
		 * @param {LDAP.SearchResponse} res LDAP response manager
		 * @param {function(error: ?Error):void} next callback invoked when done or on failure
		 * @param {?Backend} backend backend to use explicitly
		 * @param {string} scope label on request's scope for use in log message
		 * @param {LDAPServer} ldapServer current server
		 * @returns {void}
		 */
		function search( req, res, next, backend, scope, ldapServer ) {
			const boundAs = req.connection.ldap.bindDN;

			boundAs.setFormat( { skipSpace: true } );

			debug( `${req.logId}: searching for ${req.filter.toString()} in ${scope} as ${boundAs}` );

			let usernames;

			try {
				usernames = ldapServer.constructor.extractUsernames( req.filter, backend );
			} catch ( cause ) {
				const msg = `processing failed: ${cause.message}`;

				error( `${req.logId}: ${msg}` );
				next( new LDAP.OperationsError( msg ) );
				return;
			}

			debug( `${req.logId}: filter is testing for ${usernames.length} username(s)` );

			const boundUser = boundAs.equals( "" ) || boundAs.equals( "cn=anonymous" ) ? undefined : ldapServer.constructor.dnToUsername( boundAs );

			if ( usernames.length < 1 && boundUser ) {
				usernames.push( boundUser );
			}

			if ( usernames.length === 1 ) {
				const user = usernames[0];
				const { attribute, mailbox, domain: mailboxDomain } = user;

				if ( !boundUser || ( boundUser.mailbox === mailbox && boundUser.domain === mailboxDomain ) ) {
					const _backend = backend || backends.selectByName( mailboxDomain );

					if ( _backend && _backend.coversDomain( mailboxDomain ) ) {
						ldapServer.checkPeer( req, _backend )
							.then( permitted => {
								if ( permitted ) {
									const dn = `${attribute}=${mailbox},${ldapServer.constructor.domainToDN( _backend.domain )}`;

									debug( `${req.logId}: searching for ${req.filter.toString()} yielded match ${dn}` );

									res.send( {
										dn,
										attributes: _backend.qualifyAttributes( user, boundUser, ldapServer.constructor.qualifyAttributes( user, boundUser, {
											objectclass: [ "top", "user" ],
											[attribute]: mailbox,
										} ) ),
									} );

									res.end();
									next();
								} else {
									next( new LDAP.InsufficientAccessRightsError( "client rejected" ) );
								}
							} )
							.catch( cause => {
								next( new LDAP.OperationsError( cause.message ) );
							} );

						return;
					}
				}
			}

			res.end();
			next();
		}
	}

	/**
	 * Tests if current client is permitted to send any request.
	 *
	 * @param {LDAP.SearchRequest|LDAP.BindRequest} req request descriptor
	 * @param {Backend} backend backend selected for processing the request
	 * @returns {Promise<boolean>} promises true if peer is permitted to send request and false otherwise
	 */
	checkPeer( req, backend ) {
		let peers = this.acceptedPeers;

		if ( backend && backend.acceptedPeers ) {
			peers = peers.concat( backend.acceptedPeers );
		}

		if ( !peers || !peers.length ) {
			debug( "empty list of accepted peers -> accepting all" );

			return Promise.resolve( true );
		}

		const ip = req.connection.remoteAddress;

		return DNS.reverse( ip )
			.then( ( [name] ) => {
				const isListed = name && peers.some( peer => peer === name );

				debug( `peer ${ip} is ${name} which is ${isListed ? "" : "not "}listed -> ${isListed ? "accept" : "reject"}` );

				return isListed;
			} );
	}
}


exports.LDAPServer = LDAPServer;
