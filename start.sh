#!/bin/sh

if [ $# -ge 1 ]; then
	exec "$@"
fi

set -eu

[ -z "$(awk '$2~/^\/config/' /proc/mounts)" ] && {
	echo "missing volume mounted in /config" >&2
	exit 1
}

[ -f /config/config.js ] || {
	cp config.dist.js /config/config.js
	echo "created configuration in mounted volume - please review it before restart" >&2
	exit 1
}

DEBUG="${DEBUG:-*:alert,*:error,*:warn,*:info}" CONFIG_FILE=/config/config.js node server
