"use strict";

module.exports = {
	/**
	 * LDAP configuration
	 */
	server: {
		/**
		 * IP address for binding listener socket
		 */
		ip: process.env.LISTEN_IP,

		/**
		 * port number of listener socket
		 */
		port: process.env.LISTEN_PORT,

		/**
		 * filename of certificate in PEM format, may be relative to this file
		 */
		certFile: process.env.TLS_CERT || "/path/to/cert.pem",

		/**
		 * filename of key in PEM format, may be relative to this file
		 */
		keyFile: process.env.TLS_KEY || "/path/to/key.pem",
	},

	/**
	 * Maps domains into backend selectors.
	 *
	 * Users are expected to log in with a mail address. Its domain part is used
	 * to pick a backend from this map. Either backend selector is choosing a
	 * protocol and optionally some backend host to address instead of domain
	 * name found in mail address.
	 *
	 * Supported backends:
	 *
	 * pop3 or pop3s      - always encrypted POP3
	 */
	backends: {
		// picks POP3 backend with particular host to be used
		"example.com": "pop3s://mx.example.com",
		// picks POP3 backend with selected name foo.com used implicitly
		"foo.com": "pop3s",
		// picks POP3 backend with option `short` applied (causing local part of
		// mail address used as username at POP3, only)
		"bar.com": "pop3s://short@bar.com",
		// illustrates extended use of options: there is `short` option again as
		// well as two clients' hostnames for limiting this backend to those
		// LDAP clients ... the order of options doesn't matter
		"baz.com": "pop3s://short:consumer1.baz.com:consumer2.baz.com@baz.com",
	},

	/**
	 * Optionally lists domain names of clients accepted, only.
	 *
	 * If this list is empty (and there is no such list provided on processing
	 * backend) all client requests are accepted. Otherwise a client's IP is
	 * reversed into the host's name by looking up its PTR RR in DNS. The
	 * client's request is accepted if the resulting name is listed here, only.
	 * Otherwise it is rejected.
	 */
	peers: [
		// "consumer.foo.com",
	],
};
