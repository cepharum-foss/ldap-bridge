FROM node:lts-alpine

WORKDIR /app

COPY . /app
RUN chmod +x /app/start.sh && npm ci

EXPOSE 389
EXPOSE 636
VOLUME /config

ENTRYPOINT ["/app/start.sh"]
