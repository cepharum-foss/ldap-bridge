/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const LDAP = require( "ldapjs" );
const { POP3Client } = require( "@cepharum/pop3-client" );

const { Backend } = require( "./abstract" );
const { debug, error } = require( "../lib/logger" );

/**
 * Implements backend for authenticating user against remote POP3 service.
 *
 * @property {object<string,boolean>} flags set of boolean switches customizing backend's behaviour
 * @property {string[]} acceptedPeers optional list of LDAP clients accepted to query this backend, if empty all clients are accepted
 */
class POP3Backend extends Backend {
	/**
	 * @param {*} dsn backend configuration, usually provided as DSN string
	 * @param {string} domain provides domain this backend has been associated with in configuration
	 */
	constructor( dsn, domain ) {
		super();

		Object.defineProperties( this, {
			domain: { value: String( domain ).trim().toLowerCase(), enumerable: true },
		} );


		const match = this.constructor.parse( dsn );
		if ( !match ) {
			throw new TypeError( "invalid backend definition" );
		}

		const options = {
			host: match[2] || this.domain,
			port: parseInt( match[2] && match[3] ) || 995,
			ssl: true,
		};

		const peers = [];
		const flags = {
			short: false,
		};

		if ( match[1] ) {
			for ( const option of match[1].trim().toLowerCase().split( /[,:\s]+/ ) ) {
				if ( option.indexOf( "." ) > -1 ) {
					peers.push( option );
				} else {
					const value = option[0] !== "-";
					const name = !value || option[0] === "+" ? option.slice( 1 ) : option;

					flags[name] = value;
				}
			}
		}

		Object.defineProperties( this, {
			options: { value: Object.freeze( options ), enumerable: true },
			flags: { value: Object.freeze( flags ), enumerable: true },
			acceptedPeers: { value: Object.freeze( peers ), enumerable: true },
		} );
	}

	/** @inheritDoc */
	static parse( dsn ) {
		return /^pop3s?(?::(?:\/\/)?(?:([^/?#%@]+)@)?([^/:?#%@]+)(?::(\d+))?)?$/i.exec( dsn );
	}

	/** @inheritDoc */
	coversDomain( domain ) {
		return domain === this.domain;
	}

	/** @inheritDoc */
	exists( name ) { // eslint-disable-line no-unused-vars
		return Promise.resolve( true );
	}

	/** @inheritDoc */
	authenticate( username, password ) {
		debug( `authenticating against POP3 at ${this.options.host} as ${username}` );

		const client = new POP3Client( Object.assign( {}, this.options, {
			username: this.flags.short ? username.replace( /@.*$/, "" ) : username,
			password,
		} ) );

		return client.login()
			.catch( cause => {
				error( `POP3 login failed at ${client.host}: ${cause.message}` );
				throw new LDAP.OperationsError( cause.message );
			} )
			.then( loggedIn => {
				if ( !loggedIn ) {
					throw new LDAP.InvalidCredentialsError( "invalid credentials" );
				}
			} )
			.finally( () => client.close() );
	}
}

exports.POP3Backend = POP3Backend;
