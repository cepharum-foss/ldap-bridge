/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const { info, warn } = require( "../lib/logger" );
const { POP3Backend } = require( "./pop3" );

const { Backend } = require( "./abstract" );

const AvailableBackends = [POP3Backend];

/**
 * @typedef {object} BackendInformation
 * @property {function(name: string):boolean} selector callback detecting whether backend is processing given user or users of given domain
 * @property {string} domain backend's domain used in service configuration
 * @property {Backend} backend backend manager
 */

/**
 * Aggregates all available backends for looking up backend for given name.
 *
 * @property {Array<BackendInformation>} available available backends
 */
class Backends extends Backend {
	/**
	 * @param {object} config service configuration
 	 */
	constructor( config = {} ) {
		super();

		const { backends: configuredBackends = {} } = config;
		const available = [];

		Object.keys( configuredBackends ).forEach( backendDomain => {
			const backendSelector = configuredBackends[backendDomain];
			const BackendImpl = AvailableBackends.find( candidate => candidate.parse( backendSelector ) );

			if ( BackendImpl ) {
				info( `backend ${BackendImpl.name} is available for ${backendDomain}` );

				available.push( {
					selector: name => name === backendDomain || name.endsWith( `@${backendDomain}` ),
					domain: backendDomain,
					backend: new BackendImpl( backendSelector, backendDomain ),
				} );
			} else {
				warn( `invalid backend Selector: ${backendDomain} => ${backendSelector}` );
			}
		} );

		Object.defineProperties( this, {
			available: { value: Object.freeze( available ), enumerable: true },
		} );
	}

	/**
	 * Picks available backend configured for handling selected user name.
	 *
	 * @param {string} name name of user to process
	 * @returns {Backend|undefined} found backend for processing selected user name, undefined on missing backend
	 */
	selectByName( name ) {
		for ( const { selector, backend } of this.available ) {
			if ( selector( name ) ) {
				return backend;
			}
		}

		return undefined;
	}

	/** @inheritDoc */
	exists( name ) {
		const backend = this.selectByName( name );

		return backend ? backend.exists( name ) : false;
	}

	/** @inheritDoc */
	authenticate( name, token ) {
		const backend = this.selectByName( name );

		if ( !backend ) {
			throw new Error( "no such user" );
		}

		backend.authenticate( name, token );
	}
}

exports.Backends = Backends;
