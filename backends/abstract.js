/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

/**
 * Abstract base class of all backends.
 */
class Backend {
	/**
	 * Parses backend configuration.
	 *
	 * @param {*} config backend configuration, usually provided as a DSN string
	 * @returns {object} parsed configuration, falsy on malformed configuration
	 * @abstract
	 */
	static parse( config ) { return undefined; } // eslint-disable-line no-unused-vars

	/**
	 * Detects if provided domain name is covered by current backend.
	 *
	 * @param {string} domain domain name to test
	 * @returns {boolean} true if backend is covering provided domain name
	 */
	coversDomain( domain ) { return false; } // eslint-disable-line no-unused-vars

	/**
	 * Detects if named user exists or not.
	 *
	 * @param {string} name name of user to look up
	 * @returns {Promise<boolean>} promises true if user exists, false otherwise
	 */
	exists( name ) { return Promise.resolve( false ); } // eslint-disable-line no-unused-vars

	/**
	 * Tries to authenticate named user against matching backend with given
	 * token.
	 *
	 * @param {string} name name of user to authenticate
	 * @param {string} token secret credential suitable for authenticating as selected user
	 * @returns {Promise<void>} promises user successfully authenticated
	 */
	authenticate( name, token ) { return Promise.reject( new Error( "no backend" ) ); } // eslint-disable-line no-unused-vars

	/**
	 * Qualifies attributes of entry to be returned to a searching LDAP client.
	 *
	 * @param {object} user information on user described by returned entry
	 * @param {object} boundUser information on currently bound LDAP user
	 * @param {object} attributes LDAP attributes to be qualified
	 * @returns {object} qualified LDAP attributes
	 */
	qualifyAttributes( user, boundUser, attributes ) { return attributes; }
}

exports.Backend = Backend;
